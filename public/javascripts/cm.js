(function () {})();

// var test = {
//     // ivs 1
//     /*
//     url: "https://3d26876b73d7.us-west-2.playback.live-video.net/api/video/v1/us-west-2.913157848533.channel.rkCBS9iD1eyd.m3u8",
//     type: "I",
// */
//     // ivs 2
//     /*
//     url: "https://fcc3ddae59ed.us-west-2.playback.live-video.net/api/video/v1/us-west-2.893648527354.channel.YtnrVcQbttF0.m3u8",
//     type: "I",
// */
//     // media live
//     /*
//     url: "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8",
//     type: "M",
//     */
// };

const playerObj = {
    ivsplayer: "",
    hlsplayer: "",
};


function mkParam(params, method = "get") {
    var option = {
        method: method,
        headers: {
            "Content-Type": "application/json",
            authToken: "3df648802db2952c65aea05ed93192",
        },
        body: JSON.stringify(params),
    };
    return option;
}

function loginChk() {
    if (socket.userId === "guest" || socket.nickName === "guest") {
        return false;
    } else {
        return true;
    }
}

function startArashowCountTimer() {
    var arashowStTm = $("#arashowStTm").val();

    // console.log(arashowStTm);
    var start = new Date(arashowStTm);
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var timer;

    $("#countDown").empty();
    var now = new Date();
    var distance = start - now;
    if (distance < 0) {
        clearInterval(arashowCountTimer);
        return;
    }

    var hours = Math.floor(distance / _hour);
    var minutes = Math.floor((distance % _hour) / _minute);
    var seconds = Math.floor((distance % _minute) / _second);

    var sHour = hours <= 0 ? "00:" : hours + ":";
    $("#countDown").text(sHour + twoDigits(minutes) + ":" + twoDigits(seconds));
}

function twoDigits(n) {
    return n <= 9 ? "0" + n : n;
}

function priceToString(price) {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function clickKakao() {
    var url1 = "https://i-vory.net/event/view.do?file_path=/event/shop_live&";
    var url2 = "innerPageUrl=/shop/liveView.do?seq_no=" + liveInfo.seqNo + "&seq_no=" + liveInfo.seqNo + "&link_type=ivory";
    var targetUrl = url1 + url2;

    var jellyKey = 'a5975454b5e5f80920bbf7cd12a48657';
    var ivoryKey = '1e36a67256dabc9103fc9f3137f14f6d';

    var jellyStoreLink = {
        ios:'https://itunes.apple.com/kr/app/id1503293015',
        aos:'https://play.google.com/store/apps/details?id=com.thecares.jellyview',
    }

    var ivoryStoreLink = {
        ios: 'https://itunes.apple.com/kr/app/id1113295590',
        aos: 'https://play.google.com/store/apps/details?id=com.namyang.bebe',
    }

    var liveTitle,initKey,storeLink;

    if(liveGbnCd == 'J'){
        initKey = jellyKey;
        liveTitle = '젤리쇼 일정알림';
        storeLink = {
            ios: jellyStoreLink.ios,
            aos: jellyStoreLink.aos,
        }

    }else{
        initKey = ivoryKey;
        liveTitle = '아라쇼 일정알림';
        storeLink = {
            ios: ivoryStoreLink.ios,
            aos: ivoryStoreLink.aos,
        }
    }

    shareKakao(targetUrl, liveTitle, liveInfo.liveTitle, liveInfo.shareImgUrl, initKey, storeLink);
}

// cm.js liveGbnCd 값 전달 후 아이보리/젤리뷰 비교하여 공유
function copyUrl(){

    // 기존의 share.js copyUrl 함수
    copyUrlFunc(liveGbnCd);
}

function arashowTimer(srcUrl, type) {
    if (type == null) type = "I";
    // console.log(srcUrl, type);
    let today = new Date();
    var liveStart = new Date($("#arashowStTm").val());
    var liveEnd = new Date($("#arashowEndTm").val());

    console.log("timer");

    /* live show pendding */
    if (today < liveStart) {
        console.log("pendding");

        $('#beforeWaitingBox').attr('hidden',false);
        
        $(".before_item").attr("hidden", false);
        $(".after_item").attr("hidden", true);
        $("#on_live").attr("hidden", true);
        // 아라쇼 남은시간 카운트 다운 타이머 시작

        if (arashowCountTimer == 0) {
            arashowCountTimer = setInterval(startArashowCountTimer, 1000);
        }
    } else if (today > liveStart && today < liveEnd) {
        console.log("ing");

        /* live ing .*/
        $('#beforeWaitingBox').attr('hidden',true);
        $(".before_item").attr("hidden", true);
        $(".after_item").attr("hidden", true);
        $("#on_live").attr("hidden", false);
        $("#live_preview").css("display", "none");
        var video = document.getElementById("video-player");
        var videoml = document.getElementById("video-player_ml");
        var cntUrl = document.getElementById("currentUrl");

        // console.log(
        //     "ivs on",
        //     cntUrl.getAttribute("data-ivs"),
        //     "hls on",
        //     cntUrl.getAttribute("data-ml"),
        //     "\ncurrent url",
        //     cntUrl.value,
        //     "\nget url",
        //     srcUrl,
        //     "\nsame?",
        //     cntUrl.value == srcUrl
        // );
        if (cntUrl.value != srcUrl) {
            if (type == "I") {
                video.style.display = "block";
                videoml.style.display = "none";
                if (IVSPlayer.isPlayerSupported) {
                    if (!cntUrl.getAttribute("data-ivs")) {
                        playerObj.ivsplayer = IVSPlayer.create();
                        playerObj.ivsplayer.attachHTMLVideoElement(document.getElementById("video-player"));
                    }
                    playerObj.ivsplayer.load(srcUrl);
                    playerObj.ivsplayer.play();
                    cntUrl.setAttribute("data-ivs", true);

                    // var i = 1;
                    // setInterval(() => {
                    //     console.log("check", video.readyState, playerObj.ivsplayer.core.isLoaded);
                    //     console.log(i++);
                    // }, 1000);

                    // 10초뒤 비디오 재생중이 아니면 새로고침 알럿
                    // 느린 네트워크 기준 약 30초 내에 플레이어 로드됨, 안되면 다른이유로 실패한것으로 간주하여 리로드 실행
                    // setTimeout(() => {
                    //     if (!playerObj.ivsplayer.core.isLoaded) {
                    //         if (confirm("네트워크 환경 확인후 다시 시도해주세요.\n확인을 누르면 페이지가 새로고침 됩니다.")) location.reload();
                    //     }
                    // }, 30000);
                    // setTimeout(() => {
                    //     if (video.readyState < 2) {
                    //         if (confirm("네트워크 환경 확인후 다시 시도해주세요.\n확인을 누르면 페이지가 새로고침 됩니다.")) location.reload();
                    //     }
                    // }, 10000);
                }
            } else if (type == "M") {
                video.style.display = "none";
                videoml.style.display = "block";
                if (videoml.canPlayType("application/vnd.apple.mpegurl")) {
                    console.log("not supported");
                    videoml.src = srcUrl;
                    //
                    // If no native HLS support, check if hls.js is supported
                    //
                } else if (Hls.isSupported()) {
                    console.log("hls supported");
                    if (!cntUrl.getAttribute("data-ml")) {
                        playerObj.hlsplayer = new Hls();
                    }
                    playerObj.hlsplayer.loadSource(srcUrl);
                    playerObj.hlsplayer.attachMedia(videoml);

                    createOnPlay();
                }
                cntUrl.setAttribute("data-ml", true);
            }
            cntUrl.value = srcUrl;
        } else {
            console.log("src same");
            return;
        }

        clearInterval(arashowCountTimer);
        setInterval(() => {
            // 아라쇼 입장하기
        }, 30000);

        // pip test
        // document.querySelector("#test").addEventListener("click", () => {
        //     document.querySelector("#video-player").requestPictureInPicture();
        // });
    } else {
        console.log("end");

        /* live end */
        $(".before_item").attr("hidden", true);
        $(".after_item").attr("hidden", false);
        $("#on_live").attr("hidden", true);
        $("#live_preview").css("display", "block");
        clearInterval(timer);

        $('#beforeWaitingBox').attr('hidden',true);
    }
}

function formatChange(dateStr, timeStr) {
    var year = dateStr.substr(0, 4);
    var month = dateStr.substr(4, 2);
    var day = dateStr.substr(6, 2);

    var hour = timeStr.substr(0, 2);
    var min = timeStr.substr(2, 2);
    var sec = timeStr.substr(4, 2);
    return year + "-" + month + "-" + day + "T" + hour + ":" + min + ":" + sec;
}

let adminCount = 0;
async function getArashowInfo(seqNo_old) {
    try {
        var queryString = window.location.href;
        var seqNo = new URLSearchParams(location.search).get("seq_no");
        // var SVR_URL= '<%= process.env.SVR_URL %>';
        let response = await fetch(`https://i-vory.net/gateway/live/live.json?seqNo=${seqNo}`, mkParam());
        let resJson = await response.json();


        let { live } = resJson;
        live.seqNo = seqNo;
        live.queryString = queryString;
        liveInfo = live;

        // src, type추가
        // live.streamUrl
        // live.mediaType

        /* live info */
        liveGbnCd = live.liveGbnCd;
        $("#live_title").text(live.liveTitle);
        // $("#live_preview").attr("src", live.preImgUrl);

        /*
            20230525 ajy 라이브전 대기화면 퍼블리싱으로 교체
        */
        console.log('live?',live);
        var beforeHtml = '';
        var targetBox = $('#beforeWaitingBox');
        var countbox = $('#countDown').closest('.before_waiting_box');
        var liveStartTimeLabel = moment(`${live?.liveStDt}T${live?.liveStTm}`).format('A hh:mm');
        var liveEndTimeLabel = moment(`${live?.liveEndDt}T${live?.liveEndTm}`).subtract(10,'minutes').format('hh:mm');
        
        var livetimeHtml = '<div class="livetime_info_box">';
        livetimeHtml += '<p>' + liveStartTimeLabel + ' ~ ' + liveEndTimeLabel + '</p>';
        livetimeHtml += '</div>';
         
        if(!targetBox.hasClass('end')){
            if(liveGbnCd =='J'){
                targetBox.addClass('jellyshow');
                countbox.addClass('jellyshow_count');

                beforeHtml += '<div class="timer_position"></div>';

                beforeHtml += '<div class="live_box">';
                beforeHtml += '<img src="/images/liveWaiting/img_jellyshow_live.png" alt="" />';
                beforeHtml += '</div>';

                beforeHtml += '<div class="coming_soon_box">';
                beforeHtml += '<img src="/images/liveWaiting/img_jellyshow_comingsoon.png" alt="" />';
                beforeHtml += '</div>';

                beforeHtml += livetimeHtml;

                beforeHtml += '<div class="logo_box">';
                beforeHtml += '<img class="logo" src="/images/liveWaiting/img_jellyshow_logo.png" alt="" />';
                beforeHtml += '<img class="x" src="/images/liveWaiting/img_jellyshow_x.png" alt="" />';
                beforeHtml += '<p class="brand_name">' + live.liveSbj + '</p>';
                beforeHtml += '</div>';
            }else{
                targetBox.addClass('arashow');
                countbox.addClass('arashow_count');

                beforeHtml += '<div class="coming_soon_box">';
                beforeHtml += '<img src="/images/liveWaiting/img_arashow_comingsoon.png" alt="" />';
                beforeHtml += '</div>';

                beforeHtml += '<div class="timer_position"></div>';

                beforeHtml += '<div class="live_box">';
                beforeHtml += '<img src="/images/liveWaiting/img_arashow_live.png" alt="" />';
                beforeHtml += livetimeHtml;
                beforeHtml += '</div>';

                beforeHtml += '<div class="logo_box">';
                beforeHtml += '<img class="logo" src="/images/liveWaiting/img_arashow_logo.png" alt="" />';
                beforeHtml += '<img class="x" src="/images/liveWaiting/img_arashow_x.png" alt="" />';
                beforeHtml += '<p class="brand_name">' + live.liveSbj + '</p>';
                beforeHtml += '</div>';
            }
            
            targetBox.addClass('end');
            if(live.preImgUrl !== '' && live.preImgUrl && live.preImgUrl !== undefined && live.preImgUrl !== null){
                targetBox.css('background', 'url("' + live.preImgUrl + '") center center / cover no-repeat');
            }
            targetBox.append(beforeHtml);
        }

        // 젤리뷰 기타 설정
        if(liveGbnCd == 'J'){
            $('#onClickProduct').addClass('jellyview');
            $('#onClickChattingMode').addClass('jellyview');
            $('.slide_title').addClass('jellyview');
            $('.inner_layout').addClass('jellyview');
        }else{
            $('#onClickProduct').removeClass('jellyview');
            $('#onClickChattingMode').removeClass('jellyview');
            $('.slide_title').removeClass('jellyview');
            $('.inner_layout').removeClass('jellyview');
        }

        /* brand info */
        $("#brand_ci").attr("src", live.supImgUrl);

        var liveStart = new Date(formatChange(live.liveStDt, live.liveStTm));
        var liveEnd = new Date(formatChange(live.liveEndDt, live.liveEndTm));

        $("#arashowStTm").val(liveStart);
        $("#arashowEndTm").val(liveEnd);
        $("#streamUrl").val(live.streamUrl);

        if(liveGbnCd == 'J'){
            $("#live_platform").attr('src', '/images/jellyshow-logo_basic.png');
        } else {
            $("#live_platform").attr('src', '/images/logo.png');
        }
        
        arashowTimer(live.streamUrl, live.mediaType);
        // test url
        // arashowTimer(test.url, test.type);

        startGa(live.liveTitle);
        // timer = setInterval(arashowTimer, 15000);

        // 관리자면 .. 라이브 방송 데이터를 최대한 상세하게 보낸다
        if (queryString.toString().includes("admin") && adminCount === 0) {
            adminCount = 1;

            live.adminGbnCd = "admin";
            // console.log(count);
            socket.emit("sendLiveInfoToAdm", {
                data: live,
                // title:
            });
        }
    } catch (e) {}
    finally{
        $('#live_platform').closest('h1').css('opacity', 1);
    }
}

// 상품정보 타이머
function getProdtInfo(seqNo) {
    refreshProdtInfo(seqNo);
    var prdtTimer = setInterval(function () {
        refreshProdtInfo(seqNo);
    }, 15000);
}

// 상품정보 api
async function refreshProdtInfo(seqNo) {
    // var SVR_URL= '<%= process.env.SVR_URL %>';
    let response = await fetch(`https://i-vory.net/gateway/live/product.json?liveSeqNo=${seqNo}`, mkParam());
    let resJson = await response.json();

    let { productList } = resJson;

    var prdtCnt = 0;
    if (productList != null || productList.length > 1) {
        prdtCnt = productList.length;
    }

    if (prdtCnt <= 0) {
        // $("#mainPrdt").addClass("hide");
        $("#onClickProduct").addClass("hide");
        return;
    }
    $("#count").text(prdtCnt);

    appendPrdtList(productList);
    productList
        .filter((item) => {
            return item.pdtGbnCd == "Y";
        })
        .forEach((item) => {
            $("#thumImg").attr("src", item.pdtImg);
            $("#tagItemTitle").text(item.pdtNm);
            $("#tagRate").text(item.dcRate + "%");
            $("#tagPrice").text(priceToString(item.dcPrice) + "원");

            document.getElementById("mainPrdt").onclick = function () {
                clickPrdt(item.pdtUrl);
            };
        });
}

function appendPrdtList(live) {
    $("#prdtList").empty();

    // var live = oldLive.filter(item =>{
    //   return item.pdtGbnCd != "Y";
    // });

    var html = "";

    if (live == null || live.length <= 0) {
        return;
    }

    for (var i = 0; i < live.length; i++) {
        html += "<li onclick=\"clickPrdt('" + live[i].pdtUrl + "');\">";
        html += '   <div class="thumbnail">';
        html += '        <img src="' + live[i].pdtImg + '" alt="' + live[i].pdtNm + '" />';
        html += "    </div>";
        html += '    <a target="_blank" class="product_info">';
        html += '        <strong class="name">' + live[i].pdtNm + "</strong>";
        html += '        <span class="price">';
        html += '            <span class="rate">' + live[i].dcRate + "%</span>";
        html += "            <strong>" + priceToString(live[i].orPrice) + "</strong>원";
        html += "        </span>";
        html += '        <span class="dc"> 최대 할인가 <strong>' + priceToString(live[i].dcPrice) + "</strong>원 </span>";
        html += '        <span class="store">' + live[i].brdNm + "</span>";
        html += "    </a>";
        html += "</li>";
    }
    $("#prdtList").append(html);
}

function clickPrdt(url) {
    try {        
        if(liveGbnCd === 'J'){
            window.ReactNativeWebView.postMessage(JSON.stringify({callType : 'prdtDetail' , prdtCd : url}));
            setTimeout(()=>{
                location.href = url;
            }, 600);
            return;
        }
        
        if (isAppRunning()) {
            let controller = navigator.userAgent;
            if (controller.includes("_second")) {
                location.href = url;
            } else {
                window.parent.postMessage(
                    {
                        functionName: "moveLivePrdt",
                        url: url,
                    },
                    "*"
                );
            }
            // parent.moveLivePrdt(link);
        } else {
            location.href = url;
            // var name = '_blank';
            // var specs = "width=420,height=820";
            // var newWindow = window.open(link, name, specs);
        }
    } catch (e) {
        console.log("message : " + e);
    }
}

function isAppRunning() {
    var UserAgent = navigator.userAgent;
    if (UserAgent.indexOf("ivory_3.0") > 0) {
        console.log("앱 접속증입니다.");
        return true;
    } else {
        console.log("앱 접속이 아닙니다. agent : " + UserAgent);
        return false;
    }
}

function createOnPlay() {
    if (!isAndroidDevice()) {
        $(".btn_play").css("display", "block");
    }
}

function isAndroidDevice() {
    var bAndroidDevice = null;
    if (bAndroidDevice === null) {
        var ua = "" + navigator.userAgent;
        if (ua.toUpperCase().indexOf("IPHONE") != -1 || ua.toUpperCase().indexOf("IPAD") != -1 || ua.toUpperCase().match(/LIKE MAC OS X/i)) {
            bAndroidDevice = false;
        } else {
            bAndroidDevice = true;
        }
    }
    return bAndroidDevice;
}

function backPress() {
    try {
        
        if(liveGbnCd === 'J'){
    ////        let isJellyNative =  navigator.userAgent.includes('jellyviewWeb')? true : false;
            window.ReactNativeWebView.postMessage(JSON.stringify({callType : 'pageClose'}));
            setTimeout(()=>{
                   history.back();
               }, 600)
        }

        if (isAppRunning()) {
            let controller = navigator.userAgent;
            if (controller.includes("_second")) {
                if (isAndroidDevice()) {
                    location.href = "ivory://dismisspage";
                } else {
                    location.href = "ivory://dismissShop";
                }
            } else {
                window.parent.postMessage(
                    {
                        functionName: "parentPressBack",
                    },
                    "*"
                );
                parent.parentPressBack();
            }
        } else {
            history.back();
        }
    } catch (e) {
        console.log("message : " + e);
    }
}

function setCookie(name, value, expiredays) {
    var todayDate = new Date();
    todayDate.setDate(todayDate.getDate() + expiredays);
    document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";";
}

function getCookie(cookie_name) {
    // return document.cookie;
    var x, y;
    var val = document.cookie.split(";");

    for (var i = 0; i < val.length; i++) {
        x = val[i].substr(0, val[i].indexOf("="));
        y = val[i].substr(val[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == cookie_name) {
            return unescape(y);
        }
    }
}

function animateHeart() {
    // A.currentTime = 0.09;
    // A.play();
    TweenMax.fromTo([bigHeart, shadow], 0.15, { scale: 1 }, { scale: 0.88, repeat: 1, yoyo: true, ease: Back.easeIn.config(7) });
    var newH = document.createElement("img"),
        newC = document.createElement("div"),
        WH = R(65, 12),
        newDs = [];
    for (var nd, i = 2; i--; ) {
        nd = document.createElement("div");
        nd.className = "dots";
        newDs.push(nd);
        container.insertBefore(nd, bigHeart);
    }
    newH.src = heartSrc;
    newH.className = "hearts";
    newC.className = "circ";
    TweenLite.set(newH, { width: WH, height: WH });
    container.insertBefore(newH, bigHeart);
    container.appendChild(newC);
    heartAnim(newH, newC, newDs);
}

function showLoading() {
    var loading = "";
    $("body").bind("touchmove", function (e) {
        e.preventDefault();
    });

    // loading += '<div class="loading_wrap">';
    // loading += '     <img class="loading-circle" src="/images/loading-circle.png" alt="Loading..."/>';
    // loading += '     <img class="loading_point" src="/images/loading_point.png" alt="Loading..."/>';
    // loading += "</div>";
    $("body").append(setLoadingIcon(loading));
    /* 터치막으면 스크롤이 top 0으로 가는 버그 떄문에 주석 */
    //$("html, body").css({overflow: 'hidden'}).bind('touchmove'); //브라우져에 터치를 막아 스크롤
    var timer = setInterval(function () {
        hideLoading();
        clearInterval(timer);
    }, 3500);
}

function showLoadingInfinity(strArray) {
    var loading = "";
    $("body").bind("touchmove", function (e) {
        e.preventDefault();
    });

    // loading += '<div class="loading_wrap">';
    // loading += '     <img class="loading-circle" src="/images/loading-circle.png" alt="Loading..."/>';
    // loading += '     <img class="loading_point" src="/images/loading_point.png" alt="Loading..."/>';
    // loading += "</div>";
    $("body").append(setLoadingIcon(loading));
    var index = 0;
    var timer = setInterval(function () {
        try {
            $("#change_text").text(strArray[index]);
            index++;
        } catch (e) {}
    }, 1200);

    return timer;
}

function showLoadingInterval(time) {
    var loading = "";
    $("body").bind("touchmove", function (e) {
        e.preventDefault();
    });

    $("body").append(setLoadingIcon(loading));
    // $("html, body").css({overflow: 'hidden'}).bind('touchmove');
    var timer = setInterval(function () {
        hideLoading();
        clearInterval(timer);
    }, time);
}
/*
  since  : 2020-11-20 오후 1:22
  func   : 로딩 프로그레스 삭제
  des    : 최소지연시간 0.6 초
*/
function hideLoading() {
    setTimeout(function () {
        $(".loading_wrap").remove();
        $("body").unbind("touchmove");
        $("html, body").css({ overflow: "visible" }).unbind("touchmove");
    }, 600);
}

function startGa(title) {
    document.title = title;
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag("js", new Date());
    gtag("config", "UA-211983524-1");
}

async function refreshDisconnectList(resJson) {
    $("#dcList").empty();

    var html = "";

    if (resJson == null || resJson.length <= 0) {
        return;
    }

    for (var i = 0; i < resJson.length; i++) {
        html += "<li>";
        html += "   <strong>";
        html += '       <label for="listLi" class="name">';
        html += resJson[i].replaceAll('"', "");
        var custId = resJson[i].replaceAll('"', "");
        html += '           <input class="name" value="' + custId + '" type="checkbox" id="listLi" name="chk" />';
        html += "       </label>";
        html += "   </strong>";
        html += "</li>";

        console.log(resJson[i].replaceAll('"', ""));
    }

    $("#dcList").append(html);
}
async function forceDisconnectList() {
    let response = await fetch("/getAdminForceDisconnect");
    let disconnectList = await response.json();

    return disconnectList;
}


function setLoadingIcon(loading){

    loading += '<div class="loading_wrap">';
    if(liveGbnCd == 'J'){
        loading += '     <img class="loading_point jelly" src="/images/jellyshow_loading-icon.gif" alt="Loading..." />';
    }else{
        loading += '     <img class="loading-circle" src="/images/loading-circle.png" alt="Loading..."/>';
        loading += '     <img class="loading_point" src="/images/loading_point.png" alt="Loading..."/>';
    }
    loading += "</div>";

    return loading;
}

function logDateFormat(date) {
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let hour = date.getHours();
    let minute = date.getMinutes();
    let second = date.getSeconds();

    month = month >= 10 ? month : '0' + month;
    day = day >= 10 ? day : '0' + day;
    hour = hour >= 10 ? hour : '0' + hour;
    minute = minute >= 10 ? minute : '0' + minute;
    second = second >= 10 ? second : '0' + second;

    return `/filedown/${date.getFullYear()}-${month}-${day}.chat.log`;
}
