function shareKakao(link, title, des, imgUrl, initKey, storeLink) {
    try {
        showLoadingInterval(2000);
        Kakao.init(initKey);
    } catch (e) {}

    Kakao.Link.sendDefault({
        objectType: 'feed',
        content: {
            title: title,
            description: des,
            imageUrl: imgUrl,
            link: {
                mobileWebUrl: link,
                webUrl: link
            }
        },
        buttons: [
            {
                title: '안드로이드 다운받기',
                link: {
                    mobileWebUrl: storeLink.aos,
                    webUrl: storeLink.aos
                }
            }, {
                title: '아이폰 다운받기',
                link: {
                    mobileWebUrl: storeLink.ios,
                    webUrl:  storeLink.ios
                }
            }
        ]
    });
}

function copyUrlFunc(gbnCd){
    showLoadingInterval(2000);
    var type = '';
    if(gbnCd == 'J'){
        type = 'shop_jellylive';
    }else{
        type='shop_live';
    }
    var url1 = 'https://i-vory.net/event/view.do?file_path=/event/' + type + '&'
    var url2 = 'innerPageUrl=/shop/liveView.do?seq_no=' +liveInfo.seqNo +'&seq_no=' +liveInfo.seqNo +'&link_type=ivory'

    var targetUrl = url1+url2;
    // var targetUrl = 'https://i-vorylive.net?seq_no=' + liveInfo.seqNo;
    var temp = document.createElement("textarea");
    document.body.appendChild(temp);
    temp.value = targetUrl;
    temp.select();
    temp.setSelectionRange(0, 9999);

    document.execCommand('copy');
    document.body.removeChild(temp);
    var platform;
    if(gbnCd == 'J') platform = '젤리쇼';
    else platform = '아라쇼';
    alert(platform + ' 공유 링크가 클립보드에 복사되었습니다.');
}
