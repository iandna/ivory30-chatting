var express = require("express");
var router = express.Router();
const logger = require("../winston")("server");
const fetch = require('node-fetch');

var fs = require('fs');
var path = require('path');
var mime = require('mime');


const request = require("request-promise-native");
const flash = require("express-flash");
const passport = require("passport");
const initPassport = require("../config/passport");
router.use(passport.initialize());
router.use(passport.session());
router.use(flash());
initPassport(passport);
const checkAuth = (req, res, next) => {
    if(req.session.loginData != ""){
        return next();
    }
    var seq = req.query.seq_no;
    res.redirect("/adminLogin?seq_no=" + seq);
}; 

require("dotenv").config();
router.get("/", function (req, res, next) {
    res.render("index", { title: "아라쇼 메인" });
});

router.get("/admin", checkAuth, function (req, res, next) {
    res.render("admin", { title: "아라쇼 라이브 방송 관리자" });
});

router.get("/host", checkAuth, function (req, res, next) {
    res.render("host", { title: "아라쇼 라이브 방송 호스트" });
});

router.post("/onlogin", (req, res, next) => {
 
    var seq = req.body.seq;
    let mngId = req.body.id;
    let pwd = req.body.password;
    fetch(`https://i-vory.net/gateway/live/verifyAuth.json`, {
        method: 'post',
        headers: {
            "Content-Type": "application/json",
            authToken: "3df648802db2952c65aea05ed93192",
        },
        body: JSON.stringify( {mngId , pwd }),
    }).then(response => response.json())
    .then(response => { 
        console.log(response);
        if(response.resCd === "000"){ 
            
            let {mngId, mngNickname, mngColor, isAdmin } = response.result.mngInfo;
            // isAdmin 1: 마스터, 0: 공급사
            const permission = isAdmin === 1 ? 'master' : 'provider';

            req.session.loginData = {id:mngId, name : mngNickname, textColor: mngColor, permission: permission}; 
            var ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;

            console.log('live, isAdmin',isAdmin);

            var logDate = new Date();

            var year = logDate.getFullYear();
            var month = ("0" + (logDate.getMonth() + 1)).slice(-2);
            var day = ("0" + logDate.getDate()).slice(-2);

            var logDateString = year + "-" + month + "-" + day + " " + String(logDate).substr(16, 8);
            var nowDateString = year + month + day + String(logDate).substr(16, 8).replace(/:/gi, "");

            logger.info(`[ADMIN_LOGIN] ip:${ip},  id:${mngId}, date:${logDateString}`);
            console.log("process.env.NODE_ENV : " + process.env.NODE_ENV);

            var param = new Object();

            param.time = logDateString;
            param.id = mngId;
            param.nickName = mngNickname;
            param.color = mngColor;
            param.ip = ip;

            var requestParam = JSON.stringify(param);
            if (process.env.NODE_ENV === "production") {
                request("https://mrygyg99vl.execute-api.ap-northeast-2.amazonaws.com/adm/login/set", {
                    method: "post",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: requestParam,
                });
            }
 
            let color = mngColor.replace('#', '');
            if (mngId === "livehost") {
                res.redirect(`/host?seq_no=${seq}&adminId=${mngId}&nickName=${mngNickname}&color=${color}&permission=${permission}`);
            } else {
                res.redirect(`/admin?seq_no=${seq}&adminId=${mngId}&nickName=${mngNickname}&color=${color}&permission=${permission}`);
            }
        } else {
            res.redirect("/adminLogin?seq_no=" + seq + "&error=noid");    
        }
    })
    .catch(err => {
        console.log( err);
        res.redirect("/adminLogin?seq_no=" + seq+ "&error="+err);    
    }); 
});

/*router.post('/onlogin', passport.authenticate('local',{
    successRedirect : '/admin',
    failureRedirect : '/adminLogin',
    failureFlash : true
  })
);*/

router.get("/onlogout", (req, res) => {
    var seq = req.query.seq_no; 
    req.session.destroy(error =>{if(error) console.log(error)})
    //req.logout();
    
    res.redirect("/adminLogin?seq_no=" + seq);
});

/*router.get('/admLogin', function(req, res, next) {
  res.render('admLogin', {title: '아라쇼 라이브 방송 관리자'});
});*/

router.get("/adminLogin", function (req, res, next) {
    var seq = req.query.seq_no;
    res.render("adminLogin", { title: "아라쇼 라이브 방송 관리자", data: seq });
});

/*router.get('/loginCheck', function(req,res,next) {
  res.render('admin', { title: '아라쇼 라이브 방송 관리자' });
});*/

/*router.get('/login', function(req, res, next) {
  res.render('login', { title: '아라쇼 라이브 방송 관리자' });
});*/

router.get("/iframetest", function (req, res, next) {
    res.render("iframe", { title: "아라쇼 라이브 방송 관리자" });
});

router.get("/getAdminNotice", async function (req, res, next) {
    try {
        var client = require("../app").client;
        let resultRed = await client.lrange("admin_notice_list", 0, -1, function (err, items) {
            if (err) {
                logger.error(`error log : ${err} getAdminNotice`);
                res.json("err", "0001");
            }
            res.json(items);
        });
    } catch (e) {
        console.log(e);
    }
});

router.get("/reqListModal", async function (req, res, next) {
    res.render("listModal");
});

router.get("/getAdminForceDisconnect", async function (req, res, next) {
    // res.json({'result' : ''});
    try {
        var client = require("../app").client;
        let resultRed = await client.lrange("forceDisconnectList", 0, -1, function (err, items) {
            if (err) {
                logger.error(`error log : ${err} getAdminForceDisconnect`);
                res.json("err", "0001");
            }
            res.json(items);
        });
    } catch (e) {
        console.log(e);
    }
});

router.get("/resetForceDisconnectList", async function (req, res, next) {
    try {
        var client = require("../app").client;
        client.lrange("forceDisconnectList", 0, -1, function (err, reply) {
            if (reply) {
                reply.forEach((element, index) => {
                    client.lpop("forceDisconnectList", function (data) {
                        console.log(`data ${data}`);
                    });
                });
                res.json({ result: "ok" });
            } else {
                res.json({ result: "fail" });
            }
        });
    } catch (e) {
        console.log(e);
        res.json({ result: e });
    }
});

router.post("/removeDisconnectList", async function (req, res, next) {
    var seq = req.body.disSeqNo;

    try {
        var client = require("../app").client;
        client.lrange("forceDisconnectList", 0, -1, function (err, reply) {
            if (reply) {
                if (req.body.chk.length > 1 && req.body.chk instanceof Array) {
                    reply.forEach((element, index) => {
                        client.lrem("forceDisconnectList", 1, '"' + req.body.chk[index] + '"');
                    });
                } else {
                    client.lrem("forceDisconnectList", 1, '"' + req.body.chk + '"');
                }
            } else {
                // res.json({'result' : 'fail'});
            }
        });
    } catch (e) {
        console.log(e);
        res.json({ result: e });
    }

    res.redirect(`/admin?seq_no=${seq}`);
});

router.get("/resetNotice", async function (req, res, next) {
    try {
        var client = require("../app").client;
        client.lrange("admin_notice_list", 0, -1, function (err, reply) {
            if (reply) {
                reply.forEach((element, index) => {
                    client.lpop("admin_notice_list", function (data) {
                        console.log(`data ${data}`);
                    });
                });
                res.json({ result: "ok" });
            } else {
                res.json({ result: "fail" });
            }
        });
    } catch (e) {
        console.log(e);
        res.json({ result: e });
    }
});

// $ path.resolve('/a', 'b', 'c')
// 'C:\a\b\c'
router.get('/filedown/:file_name', function(req, res, next) {
    console.log('filedownload');
    console.log(req.params.file_name);
    __filename
    var upload_folder = path.resolve('/home', 'ec2-user', 'logs', 'chat')   
    var file = upload_folder + "/" +  req.params.file_name; // ex) /upload/files/sample.txt
    console.log(`file ${file}`);
    try {
      if (fs.existsSync(file)) { // 파일이 존재하는지 체크
        var filename = path.basename(file); // 파일 경로에서 파일명(확장자포함)만 추출
        var mimetype = mime.getType(file); // 파일의 타입(형식)을 가져옴
      
        res.setHeader('Content-disposition', 'attachment; filename=' + filename); // 다운받아질 파일명 설정
        res.setHeader('Content-type', mimetype); // 파일 형식 지정
      
        var filestream = fs.createReadStream(file);
        filestream.pipe(res);
      } else {
        res.json({ result: "error", msg : "파일을 찾을수 없습니다." });
        return;
      }
    } catch (e) { // 에러 발생시
      console.log(e);
      res.json({ result: "error" ,  msg : "파일 다운로드중 에러가 발생하였습니다."});
      return;
    }
  });





module.exports = router;
