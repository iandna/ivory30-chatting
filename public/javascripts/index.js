const GLOBAL = {
    hiddenStatus: false,
    liveStatus: 0, // 0: 방송시작전, 1: 라이브방송중, 2: 라이브종료
    FUCNTION: {
        checkHiddenItem: function () {
            const item = $("[data-hidden-item]");
            for (let i = 0; item.length > i; i++) {
                const status = $(item[i]).attr("data-hidden-item") === "false" ? false : true;
                $(item[i]).attr("data-hidden-item", !status);
            }
            $("[data-hidden-item]").attr("data-hidden-item");
        },
        timeoutRemoveClass1: function (ele, className) {
            ele.closest(".slide_wrap").removeClass("active");
            ele.closest(".slide_wrap").find(".slide_back").removeClass(className);
            ele.closest(".slide_wrap").find(".slide_box").removeClass(className);
        },
        timeoutRemoveClass2: function (ele, className) {
            ele.find(".slide_back").removeClass(className);
            ele.find(".slide_box").removeClass(className);
            $("#chattingInput").focus();
        },
        fixedScrollBottom: function () {
            // $(".chatting_wrap").scrollTop($(".chatting_wrap")[0].scrollHeight);
            $(".chatting_wrap").scrollTop(999999);
            // $('.chatting_movebox').scrollTop($('.chatting_movebox')[0].scrollHeight);
        },
        fixedScrollBottomMovebox: function (_this) {
            _this.scrollTop(999999);
        },
        fixedScrollBottomAdm: function () {
            $(".chatting_movebox").scrollTop($(".chatting_movebox")[0].scrollHeight);
        },
        repositionPopupBtn: function () {
            const btn = $("#onClickClosePopup");
            const container = $("#onClickClosePopup").closest(".img_box");
            const target = $("#onClickClosePopup").closest(".img_box").find("img");
            btn.css("right", container.width() / 2 - target.width() / 2 + btn.width() / 2);
            btn.attr("hidden", false);
        },

        showAdminNotice: async function () {
            let response = await fetch("/getAdminNotice");
            let noticeList = await response.json();

            if (noticeList == null || noticeList == undefined || noticeList.length <= 0) return;

            $("#notice_list_wrap").empty();
            let nowDate = new Date();

            noticeList.forEach((item, index) => {
                if (item == "" || item == " ") return;

                try {
                    let object = JSON.parse(item);
                    let { msg, writedTime } = object;
                    let writedDate = new Date(writedTime);
                    var diffMin = parseInt(Math.ceil(nowDate.getTime() - writedDate.getTime()) / 1000 / 60);
                    console.log(`${diffMin} 분 차이 남`);

                    var noticeTime = diffMin <= 5 ? "조금전" : `${diffMin}분전`;
                    var timeLine = document.createElement("h3");
                    timeLine.innerText = noticeTime;
                    var notice = document.createElement("div");
                    notice.innerText = msg;
                    notice.classList.add("textbox");

                    $("#notice_list_wrap").append(timeLine);
                    $("#notice_list_wrap").append(notice);
                } catch (e) {
                    return;
                }
            });

            const target = $("#slideModal_LiveInfo");
            target.addClass("active");
            target.find(".slide_back").addClass("opening");
            target.find(".slide_box").addClass("opening");
            setTimeout(GLOBAL.FUCNTION.timeoutRemoveClass2, 300, target, "opening");
        },
        chattingScrollTopMaskSetting: (_this) => {
            var wrap = _this.closest(".chatting_wrap");
            var scroll = _this.scrollTop();
            // console.log(scroll);

            if (scroll < 30) {
                wrap.addClass("scroll_top");
            } else {
                wrap.removeClass("scroll_top");
            }
        },
    },
};

$(window).on("load", function () {
    // 팝업 생성 후 클로즈 버튼 위치 초기화
    GLOBAL.FUCNTION.repositionPopupBtn();
    // 로딩 후 채팅창 하단 스크롤
    GLOBAL.FUCNTION.fixedScrollBottomMovebox($("#chattingAdminList"));
});

$(document).ready(function () {
    let likeCount = 0;

    // 알림설정
    $("#onClickAlarm, #onClickAlarm2").on("click", function () {
        alert("alarm");
    });

    $(".img_mute_off").on("click", function () {
        $("#onClickMute").removeClass("off");
        $("#onClickMute").addClass("on");
        $("#video-player").prop("muted", false);
        $(this).fadeOut();
    });

    $("#onClickPlay").on("click", function () {
        $("#onClickMute").removeClass("off");
        $("#onClickMute").addClass("on");
        $("#video-player").prop("muted", false);
        try{
            playerObj.ivsplayer.play(); 
        }catch(e){
            console.log(e);
        } 
    });

    $(".btn_play").on("click", function () {
        $("#onClickMute").removeClass("off");
        $("#onClickMute").addClass("on");
        $("#video-player").prop("muted", false);
        $(this).addClass("hide");
    });

    // 음소거
    $("#onClickMute").on("click", function () {
        if ($(this).hasClass("on")) {
            $(this).removeClass("on");
            $(this).addClass("off");
            $("#video-player").prop("muted", true);
        } else {
            $(this).removeClass("off");
            $(this).addClass("on");
            $("#video-player").prop("muted", false);
        }
    });

    // 공유
    $("#onClickShare").on("click", function () {
        const target = $("#slideModal_Share");
        target.addClass("active");
        target.find(".slide_back").addClass("opening");
        target.find(".slide_box").addClass("opening");
        setTimeout(GLOBAL.FUCNTION.timeoutRemoveClass2, 300, target, "opening");
    });

    // 세팅
    $("#onClickSetting").on("click", function () {
        $(".before_item").attr("hidden", true);
        $(".after_item").attr("hidden", false);
    });

    // 전체 닫기
    $("#onClickClose").on("click", function () {
        // alert('close');
        $(".after_item").css("display", "none");
        $(".after_item").css("display", "nlock");
    });

    // 라이브 정보 확인
    $("#chattingNoticeBottom, #onClickNotice").on("click", function () {
        // alert("");
        const target = $("#slideModal_LiveInfo");
        target.addClass("active");
        target.find(".slide_back").addClass("opening");
        target.find(".slide_box").addClass("opening");
        setTimeout(GLOBAL.FUCNTION.timeoutRemoveClass2, 300, target, "opening");
        GLOBAL.FUCNTION.showAdminNotice();
    });

    // 개인 메시지 함
    $("#onClickPriavate").on("click", function () {
        const target = $("#slideModal_private_msg");
        target.addClass("active");
        target.find(".slide_back").addClass("opening");
        target.find(".slide_box").addClass("opening");
        setTimeout(GLOBAL.FUCNTION.timeoutRemoveClass2, 300, target, "opening");
    });

    $("#onClickCoupon").on("click", function () {
        const target = $("#slideModal_coupon");
        target.addClass("active");
        target.find(".slide_back").addClass("opening");
        target.find(".slide_box").addClass("opening");
        setTimeout(GLOBAL.FUCNTION.timeoutRemoveClass2, 300, target, "opening");
    });

    // 강퇴 리스트
    $("#deleteForceDisconnectListByCheck").on("click", function () {
        const target = $("#slideModal_disconnectList");
        target.addClass("active");
        target.find(".slide_back").addClass("opening");
        target.find(".slide_box").addClass("opening");
        setTimeout(GLOBAL.FUCNTION.timeoutRemoveClass2, 300, target, "opening");
    });

    // 채팅
    $("#onClickComment").on("click", function () {
        var isFirstChat = getCookie("first_chat") != "N" ? true : false;
        if (isFirstChat) {
            setCookie("first_chat", "N", 354);
            console.log("처음이시네요");
            var img = document.createElement("img");
            img.src = "/images/qna_floating_btn.png";
            img.classList.add("qna_float");
            img.onclick = () => {
                $(img).remove();
            };
            $(".slide_box.chatting").append(img);
        }

        /* 문의하기 상태 초기화  */
        $("#onClickChattingMode").removeClass("active");

        const target = $("#slideModal_Comment");
        target.addClass("active");
        target.find(".slide_back").addClass("opening");
        target.find(".slide_box").addClass("opening");
        $(".tag_item").attr("data-hidden-item", true);
        $(".bottom_btn_group").attr("data-hidden-item", true);
        $(".chatting_wrap").css({ bottom: "90px", "z-index": 106 });
        $(".chatting_notice").css({ bottom: "265px", "z-index": 106 });
        $(".chatting_notice_bottom").css({ bottom: "90px", "z-index": 106 });
        setTimeout(GLOBAL.FUCNTION.timeoutRemoveClass2, 300, target, "opening");
    });

    // 상품 더보기
    $("#onClickProduct").on("click", function () {
        const target = $("#slideModal_Product");
        target.addClass("active");
        target.find(".slide_back").addClass("opening");
        target.find(".slide_box").addClass("opening");
        setTimeout(GLOBAL.FUCNTION.timeoutRemoveClass2, 300, target, "opening");
    });

    // 백그라운드 클릭 요소 숨김
    $("#hidden_toggler").on("click", function () {
        GLOBAL.FUCNTION.checkHiddenItem();
    });

    $("#delete_notice").on("click", async function () {
        alert();
        let response = await fetch("/resetNotice");
        let resJson = await response.json();
        console.log(resJson);
    });

    $("#onlogout").on("click", async function () {
        var seqNo = $("#onlogout").val();
        location.href = "/onlogout?seq_no=" + seqNo;
    });


    $("#onDownloadLog").on("click", async function () {
        location.href = logDateFormat(new Date());
    });

    // 강퇴리스트 전체 삭제
    $("#deleteForceDisconnectList").on("click", async function () {
        alert("강제퇴장 리스트가 전체 삭제되었습니다.");

        let response = await fetch("/resetForceDisconnectList");
        let resJson = await response.json();
    });

    // 강퇴 리스트 선택 삭제
    $("#deleteForceDisconnectListByCheck").on("click", async function () {
        let response = await fetch("/getAdminForceDisconnect");
        let resJson = await response.json();

        await refreshDisconnectList(resJson);
    });

    // slide close
    $(".onclickCloseSlide, .slideModalBackground").on("click", function () {
        $(this).closest(".slide_wrap").find(".slide_back").addClass("closing");
        $(this).closest(".slide_wrap").find(".slide_box").addClass("closing");
        $(".tag_item").attr("data-hidden-item", false);
        $(".bottom_btn_group").attr("data-hidden-item", false);

        if (!location.href.includes("admin")) {
            $(".chatting_wrap").css({ bottom: "90px", "z-index": 101 });
            $(".chatting_notice").css({ bottom: "260px", "z-index": 101 });
            $(".chatting_notice_bottom").css({ bottom: "90px", "z-index": 101 });
        }
        setTimeout(GLOBAL.FUCNTION.timeoutRemoveClass1, 300, $(this), "closing");
    });

    // tab change
    $("[class^=tab_btn]").on("click", function () {
        $(this).parent(".tab_group").find("[class^=tab_btn]").removeClass("active");
        $(this).addClass("active");
        $(this).closest(".slide_box").find(".slide_content").removeClass("active");
        $;
        $(this)
            .closest(".slide_box")
            .find("[data-tab=" + $(this).attr("data-tab-btn").split("_").pop() + "]")
            .addClass("active");
    });

    // 이모티콘 클릭
    $(".emoji_item").on("click", function () {
        var message = $(this).find("span").text();
        var oldMsg = chattingInput.value;
        chattingInput.value = oldMsg + message;
        // $('#chattingInput').text(oldMsg+message);
        // $("#onClickChattingSubmit").trigger("click");
        // socket.emit('sendMessage', {
        //     message
        // });
        chattingInput.focus();
    });
    // 질문모드 변경
    $("#onClickChattingMode").on("click", function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $("#chattingInput").attr("placeholder", "실시간 채팅에 참여하세요");
        } else {
            $(this).addClass("active");
            $("#chattingInput").attr("placeholder", "궁금한 점을 질문해보세요");
        }
        $("#chattingInput").focus();
    });

    // 관리자 채팅 모드
    $("#onClickChattingModeAdmin").on("click", function () {
        // if(admId != master){
        if(perm != 'master'){
            return;
        }
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $("#chattingInput").attr("placeholder", "실시간 채팅에 참여하세요");
        } else {
            $(this).addClass("active");
            $("#chattingInput").attr("placeholder", "시스템 메시지로 등록됩니다.");
        }
        $("#chattingInput").focus();
    });

    // 채팅창 높이 변경
    $(".chatting_wrap").on("click", function () {
        if (location.href.indexOf("admin") >= 0) {
            return;
        }
        if ($(this).hasClass("long")) {
            $(this).removeClass("long");
            $(".chatting_wrap").css("height", "36%");
            $(".welcome_userbox").css("bottom", "260px");
            // $(".chatting_notice").css("margin-bottom", "0px");
        } else {
            $(this).addClass("long");
            $(".chatting_wrap").css("height", "90%");
            $(".welcome_userbox").css("bottom", "400px");
            // $(".chatting_notice").css("margin-bottom", "140px");
        }
        setTimeout(function () {
            GLOBAL.FUCNTION.fixedScrollBottomMovebox($("#chattingAdminList"));
        }, 300);
    });

    // popup close
    $("#onClickClosePopup, #onClickClosePopupBack").on("click", function () {
        $(this).closest(".center_popup").css("display", "none");
    });

    // like btn default Enter
    $("#onClickLike").keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
        }
    });

    setInterval(() => (likeCount = 0), 10000);

    $("#onClickLike").on("click", function () {
        if (likeCount > 100) {
            alert("비정상적인 좋아요 클릭입니다. 좋아요가 1분 동안 제한됩니다.");
            $("#onClickLike").attr("disabled", true);
            setTimeout(() => $("#onClickLike").attr("disabled", false), 60000);
        } else {
            likeCount++;
        }
    });

    $("#chattingAdminList").on("scroll", function () {
        GLOBAL.FUCNTION.chattingScrollTopMaskSetting($(this));
    });
});
