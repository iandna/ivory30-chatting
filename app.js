console.log(process.env.NODE_ENV + " mode ");
console.log(process.env.PORT + " port ");
require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
const session = require("express-session");

const connectRedis = require("connect-redis");
const RedisStore = connectRedis(session);
const redis = require("redis");
// redis Callback to Promise lib
const bluebird = require("bluebird");
bluebird.promisifyAll(redis);
const client = redis.createClient({
    host: "localhost",
    port: 6379,
    // password: process.env.REDIS_PASSWORD,
    logError: true,
});
let garaTotalVisit = 400;
let garaviewCount = 400;
let garalikeCount = 700;

client.set("totalVisit", garaTotalVisit);
client.set("likeCount", garalikeCount);
client.set("samTimeBestCnt", 0);
client.set("viewCount", garaviewCount);

try {
    client.lrange("admin_notice_list", 0, -1, function (err, reply) {
        if (reply) {
            reply.forEach((element, index) => {
                client.lpop("admin_notice_list", function (data) {
                    console.log(`data ${data}`);
                });
            });
        } else {
            res.send("No Servers foundin the list");
        }
    });
} catch (e) {}
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var morgan = require("morgan");
const { cli } = require("winston/lib/winston/config");
const { captureRejections } = require("events");
var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

if (process.env.NODE_ENV === "production") {
    app.use(morgan("combined"));
} else {
    // development
    app.use(morgan("dev"));
}

app.use(
    session({
        httpOnly: process.env.NODE_ENV == "local" ? false: true, //자바스크립트를 통해 세션 쿠키를 사용할 수 없도록 함
        secure: process.env.NODE_ENV == "local" ? false: true, //https 환경에서만 session 정보를 주고받도록처리
        key : "loginData",
        secret: "secret key", //암호화하는 데 쓰일 키
        resave: false, //세션을 언제나 저장할지 설정함
        saveUninitialized: false, //세션이 저장되기 전 uninitialized 상태로 미리 만들어 저장
        cookie: {
            //세션 쿠키 설정 (세션 관리 시 클라이언트에 보내는 쿠키)
            httpOnly: process.env.NODE_ENV == "local" ? false: true,
            Secure: process.env.NODE_ENV == "local" ? false: true, 
        },
        store: new RedisStore({ client: client }),
    })
);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

if (process.env.NODE_ENV != "local") {
    var cors = require("cors");
    app.use(cors());
}

async function getRedis(key) {
    var test = (await client.getAsync(key)) * 1;
    console.log("in get redis.json & key : " + test);
    console.log("key : " + key + " value : " + test);
    //console.log(client);
    return test;
}

async function getRedisString(key) {
    var test = await client.getAsync(key);
    console.log("in get redis.json & key : " + test);
    console.log("key : " + key + " value : " + test);
    return test;
}

async function setRedisString(key, string) {
    await client.set(key, string);
    var test = await client.getAsync(key);
    console.log("in set redis.json" + test);
    console.log("key : " + key + " value : " + test);
    return test;
}

async function setRedis(key, count) {
    await client.set(key, (await client.getAsync(key)) * 1 + count);
    var test = await client.getAsync(key);
    console.log("in set redis.json" + test);
    console.log("key : " + key + " value : " + test);
    //console.log(client);
    return test;
}

async function setRedisList(key, object) {
    object.writedTime = new Date();
    let msgInfo = JSON.stringify(object);
    await client.lpush(key, msgInfo, redis.print);
    let resultRed = await client.lrange(key, 0, -1, function (err, items) {
        if (err) throw err;
        items.forEach(function (item, i) {
            log("list " + i + " : " + item);
        });
    });
    // client.lrem('admin_notice_list', function(err, data){
    //   console.log(data);
    // });
}

async function getRedisList(key) {
    var result = await client.lrange(key, 0, -1, function (err, items) {
        if (err) throw err;
        items.forEach(function (item, i) {
            log("list " + i + " : " + item);
        });
        return { noticelist: items };
    });
}

function log(msg, type = "log") {
    if (process.env.NODE_ENV != "produc") {
        if (type == "err") {
            console.error(`:: ${msg} ::`);
        } else {
            console.log(`:: ${msg} ::`);
        }
        console.log("------------------------------------------------------------------");
    }
}

// module.exports = app;

module.exports = {
    app: app,
    getRedis: getRedis,
    setRedis: setRedis,
    setRedisList,
    getRedisList,
    log,
    garaTotalVisit,
    garaviewCount,
    garalikeCount,
    client,
    getRedisString: getRedisString,
    setRedisString: setRedisString,
};
